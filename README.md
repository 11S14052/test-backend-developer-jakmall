# Jakmall Calculator

## Software Requirements
- Docker

## Vendor installation
```
./composer install
```
## Run the Calculator
```
./calculator
```
## Test Calculator with Phpunit
```
./vendor/bin/phpunit --testdox
```
