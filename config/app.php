<?php

return [
    'providers' => [
        \Jakmall\Recruitment\Calculator\History\CommandHistoryServiceProvider::class,
    ],
    'connections' => [
        'sqlite' => [
            'driver' => 'sqlite',
            'database' => 'jakmall.db',
            'prefix' => '',
            'foreign_key_constraints' => true,
        ],
    ],

];
