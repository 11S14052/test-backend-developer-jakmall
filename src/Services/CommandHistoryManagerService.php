<?php

namespace Jakmall\Recruitment\Calculator\Services;

use Illuminate\Database\Capsule\Manager as DB;
use Jakmall\Recruitment\Calculator\History\Libs\FileDatabase;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CommandHistoryManagerService implements CommandHistoryManagerInterface
{
    /**
     * @var string
     */
    public $driver;

    /**
     * @var string
     */
    public $pathFolder = 'storage';

    /**
     * The list of allowed log driver.
     *
     * @var array
     */
    public $resourceFiles = [
        'file'      => 'mesinhitung',
        'latest'    => 'latest',
        'composite' => 'composite'
    ];

    public function __construct()
    {
        $this->createFolderLog();
    }

    /**
     * Returns array of command history.
     *
     * @return array
     */
    public function findAll($filter = null): array
    {
        $output = [];
        $fileName = strtr($this->driver, $this->resourceFiles);
        $filePath = __DIR__ . '/../../' . $this->pathFolder . '/' . $fileName . '.log';

        if(!file_exists($filePath) && !is_file($filePath))
        {
            return array();
        }

        $f = fopen($filePath, 'r');

        $fileSize= filesize($filePath);

        if($fileSize == 0)
        {
            return array();
        }

        $read = fread($f, $fileSize);

        if (empty($read)) {
            return array();
        }

        $result=[];

        if ($read) {
            $dataArr = explode("\n", $read);
            foreach ($dataArr as $line) {
                if ($line) {
                    $lineArr = explode(",", $line);
                    $result[] = [
                        'id' => $lineArr[0],
                        'command' => $lineArr[1],
                        'operation' => $lineArr[2],
                        'result' => $lineArr[3]
                    ];
                }
            }
        }

        fclose($f);

        $output = collect($result)->toArray();

        if (!empty($filter)) {
            $output = collect($result)->where('id', $filter)->toArray();
        }

        return $output;
    }

    /**
     * Log command data to storage.
     *
     * @param mixed $command The command to log.
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($command): bool
    {
        $createData = DB::connection('sqlite')->table('log')->insertGetId($command); // save data to database sqlite
        $id = $createData;
        $newData = $id . ',' . $command['command'] . ',' . $command['operation'] . ',' . $command['result'];

        foreach ($this->resourceFiles as $key => $value) {
            $this->saveDataToLog($key, $newData, $command['operation']);
        }

        return $createData;
    }

    /**
     * Clear all data from storage.
     *
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clearAll(): bool
    {
        DB::connection('sqlite')->delete('delete from log'); // clear all data from database sqlite

        $file = glob(__DIR__ . '/../../' . $this->pathFolder . '/*');

        if (count($file)) {
            foreach ($file as $f) {
                unlink($f);
            }
        }

        return true;
    }

    /** 
     * Delete data by id from storage.
     *
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function clear($id): bool
    {
        DB::connection('sqlite')->table('log')->where('id', $id)->delete(); // clear data by id from database sqlite

        foreach ($this->resourceFiles as $val) {
            $this->removedataById($val, $id);
        }

        return true;
    }

    /**
     * Find a command by id.
     *
     * @param string|int $id
     *
     * @return null|mixed returns null when id not found.
     */
    public function find($id)
    {
        $item = DB::table('log')->where('id', $id)->first();

        if(!$item) {
            return [];
        }
        
        return [
            'id' => $item->id,
            'command' => ucwords($item->command),
            'operation' => $item->operation,
            'input' => preg_replace('#[^0-9,{}()]+#', ',', $item->operation),
            'result' => $item->result,
        ];
    }

    private function saveDataToLog($driver, $data, $operation)
    {
        $fileName = strtr($driver, $this->resourceFiles);
        $filePath = __DIR__ . '/../../' . $this->pathFolder . '/' . $fileName . '.log';

        if ($driver == "latest") {
            $this->driver = $driver;
            $allData = $this->findAll();
            $currentInfo = $this->getCurrentInfo($driver);
            $countData = isset($currentInfo['count_data']) ? $currentInfo['count_data'] : 0;

            if ($countData > 0) {
                $check = collect($allData)->where('operation',$operation)->count();

                if ($check) {
                    return NULL;
                }
            }

            $totalData = $countData - 1;

            if ($totalData >= 10) {
                $first_info = $this->getFirstInfo($driver);
                $this->removedataById($driver, $first_info[0]);
            }
        }

        file_put_contents($filePath, $data . "\n", FILE_APPEND);
    }

    protected function getCurrentInfo($driver)
    {
        $fileName = strtr($driver, $this->resourceFiles);
        $filePath = __DIR__ . '/../../' . $this->pathFolder . '/' . $fileName . '.log';

        if (!file_exists($filePath) && !is_file($filePath)) {
            return 0;
        }

        $f = fopen($filePath, 'r');

        $fileSize = filesize($filePath) ? filesize($filePath) : 1024;
        $read = fread($f, $fileSize);

        if (empty($read)) {
            return 0;
        }

        $explode = explode("\n", $read);
        $currentLine = $explode[count($explode) - 2];
        $lineArr = explode(",", $currentLine);

        return array(
            'last_id' => (int) $lineArr[0],
            'last_data' => $lineArr,
            'count_data' => (int) count($explode)
        );
    }

    private function getFirstInfo($driver)
    {
        $fileName = strtr($driver, $this->resourceFiles);
        $filePath = __DIR__ . '/../../' . $this->pathFolder . '/' . $fileName . '.log';
        $f = fopen($filePath, 'r');
        $read = fread($f, filesize($filePath));
        
        if (empty($read)) {
            return array();
        }

        $explode_new_line = explode("\n", $read);
        $first_data = $explode_new_line[0];
        $explode_data = explode(":", $first_data);

        return $explode_data;
    }

    private function removedataById($driver, $id)
    {
        $this->driver = $driver;
        $remove = collect($this->findAll())->whereNotIn('id',[$id])->toArray();
        $fileName = strtr($driver, $this->resourceFiles);
        $filePath = __DIR__ . '/../../' . $this->pathFolder . '/' . $fileName . '.log';

        file_put_contents($filePath, "");

        foreach ($remove as $row) {
            if (!empty($row['id'])) {
                $data = $row['id'] . ',' . $row['command'] . ',' . $row['operation'] . ',' . $row['result'];
                file_put_contents($filePath, $data . "\n", FILE_APPEND);
            }
        }
    }

    private function createFolderLog()
    {
        $pathFolder = __DIR__ . '/../../' .$this->pathFolder;
        
        if (!file_exists($pathFolder)) {
            mkdir($pathFolder, 0777, true);
        }
    }
}
