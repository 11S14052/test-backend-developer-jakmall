<?php

namespace Jakmall\Recruitment\Calculator\Services;

class MethodeService 
{
    /** 
     * @param array $numbers
     *
     * @return float|int
     */
    public function calculateAll(array $numbers, string $typeCalculation)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers, $typeCalculation), $number, $typeCalculation);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     * @param string $typeCalculation
     *
     * @return int|float
     */
    public function calculate($number1, $number2, $typeCalculation)
    {
        switch ($typeCalculation) {
    		case 'add':
        		return $number1 + $number2;
    			break;
    		case 'subtract':
        		return $number1 - $number2;
    			break;
    		case 'multiply':
        		return $number1 * $number2;
    			break;
    		case 'divide':
        		return $number1 / $number2;
    			break;
    		case 'power':
        		return pow($number1, $number2);
    			break;
    		default:
        		return 0;
    			break;
    	};
    }

    /**
     * @param array $numbers
     * @param string $operator
     *
     * @return string
     */
    public function generateCalculationDescription(array $numbers, string $operator): string
    {
        $glue = sprintf(' %s ', $operator);
        
        return implode($glue, $numbers);
    }
}
