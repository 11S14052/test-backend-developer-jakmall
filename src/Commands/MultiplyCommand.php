<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Services\MethodeService;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class MultiplyCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    private $historyManager;

    public function __construct(CommandHistoryManagerInterface $historyMan)
    {
        $commandVerb = $this->getCommandVerb();
        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($commandVerb));
        $this->historyManager = $historyMan;

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'multiply';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'multiplied';
    }

    public function handle(): void
    {
        $methodeService = new MethodeService();
        $numbers = $this->getInput();
        $command = $this->getCommand();
        $operator = $this->getOperator();
        $description = $methodeService->generateCalculationDescription($numbers, $operator);
        $result = $methodeService->calculateAll($numbers, $command);
        $newData = ['command' => $this->getCommandVerb(), 'operation' => $description, 'result' => $result];

        $this->historyManager->log($newData);
        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }

    protected function getCommand(): string
    {
        return $this->argument('command');
    }

    protected function getOperator(): string
    {
        return '*';
    }
}
