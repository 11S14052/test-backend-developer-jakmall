<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'history:clear {id?*} {--driver=}';

    /**
     * @var string
     */
    protected $description = 'Clear all history';

    public function __construct(CommandHistoryManagerInterface $historyManag)
    {
        $this->historyManager = $historyManag;

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history:clear';
    }

    public function handle(): void
    {
        $id = $this->argument('id')[0];

        if ($id) {
            $this->historyManager->clear($id);
            $this->info('Data with ID '. $id .' is removed');
        } else {
            $this->historyManager->clearAll();
            $this->info('All history is cleared');
        }
    }
}
