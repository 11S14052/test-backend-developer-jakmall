<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'history:list {commands?*} {--driver=}';

    /**
     * @var string
     */
    protected $description = 'Show all history';

    public function __construct(CommandHistoryManagerInterface $historyManag)
    {
        $this->historyManager = $historyManag;

        parent::__construct();
    }

    public function handle(): void
    {
        $commands = $this->argument('commands');
        $inputDriver = $this->option('driver');
        $header = ['ID', 'Command', 'Operation', 'Result'];
        $driver = $inputDriver ? $inputDriver : 'composite';

        if(empty($driver))
        {
            $this->comment('Driver not set --driver=xxxx');
        }

        $this->historyManager->driver = $driver;
        $result = $this->historyManager->findAll($commands);

        if (count($result) > 0) {
            $this->table($header, $result);
        } else {
            $this->info('History is empty.');
        }
    }
}
