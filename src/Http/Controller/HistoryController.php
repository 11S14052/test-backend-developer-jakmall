<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\JsonResponse;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\CommandHistoryServiceProvider;

class HistoryController
{
    private $historyManager;

    public function __construct(CommandHistoryManagerInterface $historyManag)
    {
        $this->historyManager = $historyManag;
    }

    public function index(Request $request)
    {
        $this->historyManager->driver = $request->driver ? $request->driver : 'composite';
        $allData = $this->historyManager->findAll();
        $newData = [];

        if ($allData) {
            $operator = ["+", "-", "/", "^", "*"];

            foreach($allData as $val)
            {
                $input = "[" . str_replace($operator, ",", $val['operation']) . "]";
                $newData[] = [
                                'id'=> $val['id'],
                                'command'=>$val['command'],
                                'operation'=>$val['operation'],
                                'input'=> $input,
                                'result'=>$val['result']
                            ];  
            }
        }

        return new JsonResponse($newData);
    }

    public function show($id)
    {
        $result = $this->historyManager->find($id);

        return new JsonResponse($result);
    }

    public function remove($id)
    {
        $result = $this->historyManager->clear($id);

        return new JsonResponse($result, 204);
    }
}
