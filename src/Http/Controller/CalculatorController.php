<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\Services\MethodeService;

class CalculatorController
{
    private $historyManager;

    public function __construct(CommandHistoryManagerInterface $historyManag)
    {
        $this->historyManager = $historyManag;
    }

    public function calculate(Request $request, $command)
    {
        $methodeService = new MethodeService();
    	$numbers = $request->input('input');
		$description = $this->generateCalculationDescription($numbers, $command);
    	$result = $methodeService->calculateAll($numbers, $command);
    	$response = [
                        'command' => $command, 
                        'operation' => $description, 
                        'result' => $result
                    ];
        $toInsert = [
                        'command' => $command, 
                        'operation' => $description, 
                        'result' => $result, 
                    ];

        $this->historyManager->log($toInsert);

    	return json_encode($response);
    }

    protected function generateCalculationDescription(array $numbers, $command): string
    {
        $operator = $this->getOperator($command);
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }

    protected function getOperator($command): string
    {
    	switch ($command) {
    		case 'add':
    			return '+';
    			break;
    		case 'subtract':
    			return '-';
    			break;
    		case 'multiply':
    			return '*';
    			break;
    		case 'divide':
    			return '/';
    			break;
    		case 'power':
    			return '^';
    			break;
    		default:
    			return '';
    			break;
    	};
    }
}
