<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class DivideCommandTest extends TestCase
{
    /**
     * Test the Divide Command.
     *
     * @return void
     */
    public function testDivideCommand()
    {
        $numbers = [50, 2, 5];
        $arguments = implode(" ", $numbers);
        $output = shell_exec("./calculator divide " . $arguments);
        $explodedOutput = explode(" ", $output);
        $result = $explodedOutput[count($explodedOutput) - 1];
        $actualResult = intval($result);

        echo $output . PHP_EOL;
        // Expected results
        $this->assertEquals(5, $actualResult);
    }
}
