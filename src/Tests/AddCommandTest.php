<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class AddCommandTest extends TestCase
{
    /**
     * Test the Add Command.
     *
     * @return void
     */
    public function testAddCommand()
    {
        $numbers = [1, 3, 5];
        $arguments = implode(" ", $numbers);
        $output = shell_exec("./calculator add " . $arguments);
        $explodedOutput = explode(" ", $output);
        $result = $explodedOutput[count($explodedOutput) - 1];
        $actualResult = intval($result);

        echo $output . PHP_EOL;
        // Expected results
        $this->assertEquals(9, $actualResult);
    }
}
