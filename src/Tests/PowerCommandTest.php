<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class PowerCommandTest extends TestCase
{
    /**
     * Test the Power Command.
     *
     * @return void
     */
    public function testPowerCommand()
    {
        $numbers = [3, 5];
        $arguments = implode(" ", $numbers);
        $output = shell_exec("./calculator power " . $arguments);
        $explodedOutput = explode(" ", $output);
        $result = $explodedOutput[count($explodedOutput) - 1];
        $actualResult = intval($result);

        echo $output . PHP_EOL;
        // Expected results
        $this->assertEquals(243, $actualResult);
    }
}
