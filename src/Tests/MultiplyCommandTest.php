<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class MultiplyCommandTest extends TestCase
{
    /**
     * Test the Multiply Command.
     *
     * @return void
     */
    public function testMultiplyCommand()
    {
        $numbers = [3, 4, 7];
        $arguments = implode(" ", $numbers);
        $output = shell_exec("./calculator multiply " . $arguments);
        $explodedOutput = explode(" ", $output);
        $result = $explodedOutput[count($explodedOutput) - 1];
        $actualResult = intval($result);

        echo $output . PHP_EOL;
        // Expected results
        $this->assertEquals(84, $actualResult);
    }
}
