<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class SubtractCommandTest extends TestCase
{
    /**
     * Test the Subtract Command.
     *
     * @return void
     */
    public function testSubtractCommand()
    {
        $numbers = [10, 2, 5];
        $arguments = implode(" ", $numbers);
        $output = shell_exec("./calculator subtract " . $arguments);
        $explodedOutput = explode(" ", $output);
        $result = $explodedOutput[count($explodedOutput) - 1];
        $actualResult = intval($result);

        echo $output . PHP_EOL;
        // Expected results
        $this->assertEquals(3, $actualResult);
    }
}
